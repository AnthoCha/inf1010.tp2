﻿using Inf1010.Tp2.Client.Core.Events;
using System.Collections.Generic;

namespace Inf1010.Tp2.Client.Core.Interfaces
{
    /// <summary>
    /// Interface d'un serveur pour l'utilisation par un client.
    /// </summary>
    public interface IServer
    {
        /// <summary>
        /// Lit tous les message provenant du serveur.
        /// </summary>
        void ReadMessages();

        /// <summary>
        /// Envoie le nom d'utilisateur au serveur,
        /// </summary>
        /// <param name="name"></param>
        void SendName(string name);
        /// <summary>
        /// Envoie un message aux destinataires spécifiés.
        /// </summary>
        /// <param name="to">Collection des identificateurs des destinataires du message.</param>
        /// <param name="message">Contenu du message.</param>
        void SendTo(IEnumerable<uint> to, string message);

        /// <summary>
        /// Événement de réception des clients en ligne.
        /// </summary>
        event PushEventHandler Push;
        /// <summary>
        /// Événement de réception d'un message à tous.
        /// </summary>
        event ReceiveEventHandler Receive;
        /// <summary>
        /// Événement de réception d'un message à une collection de clients.
        /// </summary>
        event ReceiveToEventHandler ReceiveTo;
    }
}
