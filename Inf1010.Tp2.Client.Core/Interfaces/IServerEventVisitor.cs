﻿using Inf1010.Tp2.Client.Core.Events;

namespace Inf1010.Tp2.Client.Core.Interfaces
{
    /// <summary>
    /// Visiteur d'événements provenant du serveur.
    /// </summary>
    public interface IServerEventVisitor
    {
        /// <summary>
        /// Visite un événement <see cref="PushEvent"/>.
        /// </summary>
        /// <param name="e">Événement.</param>
        void Visit(PushEvent e);

        /// <summary>
        /// Visite un événement <see cref="ReceiveEvent"/>.
        /// </summary>
        /// <param name="e">Événement.</param>
        void Visit(ReceiveEvent e);

        /// <summary>
        /// Visite un événement <see cref="ReceiveToEvent"/>.
        /// </summary>
        /// <param name="e">Événement.</param>
        void Visit(ReceiveToEvent e);
    }
}
