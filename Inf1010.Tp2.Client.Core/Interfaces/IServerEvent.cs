﻿namespace Inf1010.Tp2.Client.Core.Interfaces
{
    /// <summary>
    /// Événement provenant du serveur.
    /// </summary>
    public interface IServerEvent
    {
        /// <summary>
        /// Accepete un <see cref="IServerEventVisitor"/>.
        /// </summary>
        /// <param name="visitor">Visiteur d'événements provenant du serveur.</param>
        void Accept(IServerEventVisitor visitor);
    }
}
