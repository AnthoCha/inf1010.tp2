﻿using Inf1010.Tp2.Client.Core.Interfaces;
using System.Collections.Generic;

namespace Inf1010.Tp2.Client.Core.Events
{
    /// <summary>
    /// Événement de réception d'un message à une collection de destinataires.
    /// </summary>
    public class ReceiveToEvent : IServerEvent
    {
        /// <summary>
        /// Obtient l'objet à l'origine de l'événement.
        /// </summary>
        public object Sender { get; }
        /// <summary>
        /// Obtient les arguments de l'événement.
        /// </summary>
        public ReceiveToEventArgs EventArgs { get; }

        /// <summary>
        /// Initialise un événement <see cref="ReceiveToEvent"/>.
        /// </summary>
        /// <param name="sender">Objet à l'origine de l'événement.</param>
        /// <param name="e">Arguments de l'événement.</param>
        public ReceiveToEvent(object sender, ReceiveToEventArgs e)
        {
            Sender = sender;
            EventArgs = e;
        }

        /// <inheritdoc/>
        public void Accept(IServerEventVisitor visitor)
        {
            visitor.Visit(this);
        }
    }

    /// <summary>
    /// Arguments d'événements <see cref="ReceiveToEvent"/>
    /// </summary>
    public class ReceiveToEventArgs : ReceiveEventArgs
    {
        /// <summary>
        /// Obtient la collection des identificateurs des destinataires du message.
        /// </summary>
        public IEnumerable<uint> To { get; }

        /// <summary>
        /// Initialise un instance de <see cref="ReceiveToEventArgs"/>.
        /// </summary>
        /// <param name="from">Identificateur de l'émetteur du message.</param>
        /// <param name="to">Collection des identificateurs des destinataires du message.</param>
        /// <param name="message">Contenu du message.</param>
        public ReceiveToEventArgs(uint from, IEnumerable<uint> to, string message) : base(from, message)
        {
            To = to;
        }
    }

    /// <summary>
    /// Gestionnaire d'événements <see cref="ReceiveToEvent"/>.
    /// </summary>
    /// <param name="sender">Objet à l'origine de l'événement.</param>
    /// <param name="e">Arguments de l'événement.</param>
    public delegate void ReceiveToEventHandler(object Receiveer, ReceiveToEventArgs e);
}
