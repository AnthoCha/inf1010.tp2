﻿using Inf1010.Tp2.Client.Core.Interfaces;
using System;

namespace Inf1010.Tp2.Client.Core.Events
{
    /// <summary>
    /// Événement de réception d'un message à tous.
    /// </summary>
    public class ReceiveEvent : IServerEvent
    {
        /// <summary>
        /// Obtient l'objet à l'origine de l'événement.
        /// </summary>
        public object Sender { get; }
        /// <summary>
        /// Obtient les arguments de l'événement.
        /// </summary>
        public ReceiveEventArgs EventArgs { get; }

        /// <summary>
        /// Initialise un <see cref="ReceiveEvent"/>.
        /// </summary>
        /// <param name="sender">Objet à l'origine de l'événement.</param>
        /// <param name="e">Arguments de l'événement.</param>
        public ReceiveEvent(object sender, ReceiveEventArgs e)
        {
            Sender = sender;
            EventArgs = e;
        }

        /// <inheritdoc/>
        public void Accept(IServerEventVisitor visitor)
        {
            visitor.Visit(this);
        }
    }

    /// <summary>
    /// Arguments d'événements <see cref="ReceiveEvent"/>
    /// </summary>
    public class ReceiveEventArgs : EventArgs
    {
        /// <summary>
        /// Obtient l'identificateur de l'émetteur du message.
        /// </summary>
        public uint From { get; }
        /// <summary>
        /// Obtient le contenu du message.
        /// </summary>
        public string Message { get; }

        /// <summary>
        /// Initialise un instance de <see cref="ReceiveEventArgs"/>.
        /// </summary>
        /// <param name="from">Identificateur de l'émetteur du message.</param>
        /// <param name="message">Contenu du message.</param>
        public ReceiveEventArgs(uint from, string message)
        {
            From = from;
            Message = message;
        }
    }

    /// <summary>
    /// Gestionnaire d'événements <see cref="ReceiveEvent"/>.
    /// </summary>
    /// <param name="sender">Objet à l'origine de l'événement.</param>
    /// <param name="e">Arguments de l'événement.</param>
    public delegate void ReceiveEventHandler(object sender, ReceiveEventArgs e);
}
