﻿using Inf1010.Tp2.Client.Core.Interfaces;
using Inf1010.Tp2.Core.Dtos;
using System;
using System.Collections.Generic;

namespace Inf1010.Tp2.Client.Core.Events
{
    /// <summary>
    /// Événement de réception des clients en ligne.
    /// </summary>
    public class PushEvent : IServerEvent
    {
        /// <summary>
        /// Obtient l'objet à l'origine de l'événement.
        /// </summary>
        public object Sender { get; }
        /// <summary>
        /// Obtient les arguments de l'événement.
        /// </summary>
        public PushEventArgs EventArgs { get; }

        /// <summary>
        /// Initialise un événement <see cref="PushEvent"/>.
        /// </summary>
        /// <param name="sender">Objet à l'origine de l'événement.</param>
        /// <param name="e">Arguments de l'événement.</param>
        public PushEvent(object sender, PushEventArgs e)
        {
            Sender = sender;
            EventArgs = e;
        }

        /// <inheritdoc/>
        public void Accept(IServerEventVisitor visitor)
        {
            visitor.Visit(this);
        }
    }

    /// <summary>
    /// Arguments d'événements <see cref="PushEvent"/>
    /// </summary>
    public class PushEventArgs : EventArgs
    {
        /// <summary>
        /// Clients en ligne.
        /// </summary>
        public IEnumerable<ClientDto> Clients { get; }

        /// <summary>
        /// Initialise un instance de <see cref="PushEventArgs"/>.
        /// </summary>
        /// <param name="clients">Clients en ligne.</param>
        public PushEventArgs(IEnumerable<ClientDto> clients)
        {
            Clients = clients;
        }
    }

    /// <summary>
    /// Gestionnaire d'événements <see cref="PushEvent"/>.
    /// </summary>
    /// <param name="sender">Objet à l'origine de l'événement.</param>
    /// <param name="e">Arguments de l'événement.</param>
    public delegate void PushEventHandler(object sender, PushEventArgs e);
}
