﻿using Inf1010.Tp2.Core.Dtos;
using Inf1010.Tp2.Core.Enums;
using Inf1010.Tp2.Core.Events;
using Inf1010.Tp2.IO;
using Inf1010.Tp2.IO.Enums;
using Inf1010.Tp2.Server.Core.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;

namespace Inf1010.Tp2.Server.Tcp
{
    /// <summary>
    /// Implémentation d'un client à partir d'un <see cref="TcpClient"/>.
    /// </summary>
    public class TcpClientClient : IClient
    {
        private readonly TcpClient _tcpClient;
        private readonly object _writeLock = new();

        /// <inheritdoc/>
        public event EventHandler Pull;
        /// <inheritdoc/>
        public event SendEventHandler Send;
        /// <inheritdoc/>
        public event SendToEventHandler SendTo;

        /// <summary>
        /// Initialise un client avec le <see cref="TcpClient"/> spécifié.
        /// </summary>
        /// <param name="tcpClient">Client TCP.</param>
        public TcpClientClient(TcpClient tcpClient)
        {
            _tcpClient = tcpClient;
        }

        /// <inheritdoc/>
        public string ReadName()
        {
            var stream = _tcpClient.GetStream();
            using var reader = new EndianBinaryReader(stream, Encoding.UTF8, true, Endianness.Big);
            return reader.ReadString();
        }

        /// <inheritdoc/>
        public void ReadMessages()
        {
            while (_tcpClient.Connected)
            {
                var stream = _tcpClient.GetStream();
                using var reader = new EndianBinaryReader(stream, Encoding.UTF8, true, Endianness.Big);

                var toLength = reader.ReadInt32();
                var to = new uint[toLength];

                for (var i = 0; i < toLength; i++)
                {
                    to[i] = reader.ReadUInt32();
                }

                var message = reader.ReadString();

                switch (message)
                {
                    case "list":
                        Pull?.Invoke(this, EventArgs.Empty);
                        break;
                    default:
                        if (toLength > 0)
                        {
                            SendTo?.Invoke(this, new SendToEventArgs(to, message));
                        }
                        else
                        {
                            Send?.Invoke(this, new SendEventArgs(message));
                        }
                        break;
                }
            }
        }

        /// <inheritdoc/>
        public void Push(IEnumerable<ClientDto> clients)
        {
            var stream = _tcpClient.GetStream();

            lock (_writeLock)
            {
                using var writer = new EndianBinaryWriter(stream, Encoding.UTF8, true, Endianness.Big);

                writer.Write((byte)MessageType.Clients);

                writer.Write(clients.Count());

                foreach (var client in clients)
                {
                    writer.Write(client.Id);
                    writer.Write(client.Name);
                }
            }
        }

        /// <inheritdoc/>
        public void Retransmit(uint from, IEnumerable<uint> to, string message)
        {
            var stream = _tcpClient.GetStream();

            lock (_writeLock)
            {
                using var writer = new EndianBinaryWriter(stream, Encoding.UTF8, true, Endianness.Big);

                writer.Write((byte)MessageType.Message);
                writer.Write(from);
                writer.Write(to.Count());

                foreach (var toId in to)
                {
                    writer.Write(toId);
                }

                writer.Write(message);
            }
        }
    }
}
