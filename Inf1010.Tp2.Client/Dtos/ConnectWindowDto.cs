﻿namespace Inf1010.Tp2.Client.Dtos
{
    /// <summary>
    /// Objet de transfert de données de la fenêtre de connexion.
    /// </summary>
    /// <param name="Hostname">Nom de l'hôte.</param>
    /// <param name="Port">Port de l'hôte.</param>
    /// <param name="Name">Nom d'utilisateur.</param>
    public record ConnectWindowDto(string Hostname, int Port, string Name);
}
