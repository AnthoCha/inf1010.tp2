﻿using Inf1010.Tp2.Client.Models;
using System.Windows;

namespace Inf1010.Tp2.Client.Windows
{
    /// <summary>
    /// Logique d'interaction pour la fenêtre de connexion.
    /// </summary>
    public partial class ConnectWindow : Window
    {
        /// <summary>
        /// Modèle de la fenêtre de connexion.
        /// </summary>
        public ConnectWindowModel Model { get; } = new();

        /// <summary>
        /// Initialise une fenêtre de connexion.
        /// </summary>
        public ConnectWindow()
        {
            InitializeComponent();
            DataContext = Model;
        }

        /// <summary>
        /// Gestionnaire d'événement de la confirmation de la connexion.
        /// </summary>
        /// <param name="sender">Objet à l'origine de l'événement.</param>
        /// <param name="e">Arguments de l'événement.</param>
        private void OnConfirm(object sender, RoutedEventArgs e)
        {
            DialogResult = true;
            Close();
        }
    }
}
