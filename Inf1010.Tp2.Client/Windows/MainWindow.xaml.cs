﻿using Inf1010.Tp2.Client.Core.Events;
using Inf1010.Tp2.Client.Core.Interfaces;
using Inf1010.Tp2.Client.Dtos;
using Inf1010.Tp2.Client.Models;
using Inf1010.Tp2.Client.Tcp;
using Inf1010.Tp2.Client.Visitors;
using Inf1010.Tp2.Core.Events;
using System;
using System.ComponentModel;
using System.Linq;
using System.Net.Sockets;
using System.Windows;

namespace Inf1010.Tp2.Client.Windows
{
    /// <summary>
    /// Logique d'interaction pour la fenêtre principale.
    /// </summary>
    public partial class MainWindow : Window
    {
        /// <summary>
        /// Modèle de la fenêtre principale.
        /// </summary>
        public MainWindowModel Model { get; } = new();
        
        /// <summary>
        /// Événement d'envoie d'un message.
        /// </summary>
        public event SendToEventHandler SendTo;

        /// <summary>
        /// Initialise une fenêtre principale.
        /// </summary>
        public MainWindow()
        {
            InitializeComponent();
            DataContext = Model;
        }

        /// <summary>
        /// Gestionnaire d'événement de l'initiation d'une connexion.
        /// </summary>
        /// <param name="sender">Objet à l'origine de l'événement.</param>
        /// <param name="e">Arguments de l'événement.</param>
        private void OnConnect(object sender, RoutedEventArgs e)
        {
            var connectWindow = new ConnectWindow();
            var dialogResult = connectWindow.ShowDialog();

            if (dialogResult.HasValue && dialogResult.Value)
            {
                var backgroundWorker = new BackgroundWorker();
                backgroundWorker.WorkerReportsProgress = true;
                backgroundWorker.DoWork += OnDoWork;
                backgroundWorker.ProgressChanged += OnProgressChanged;
                backgroundWorker.RunWorkerCompleted += OnRunWorkerCompleted;
                backgroundWorker.RunWorkerAsync(connectWindow.Model.Dto);

                Model.Connected = true;
            }
        }

        /// <summary>
        /// Gestionnaire d'événement de l'exécution de la tâche d'arrière plan.
        /// </summary>
        /// <param name="sender">Objet à l'origine de l'événement.</param>
        /// <param name="e">Arguments de l'événement.</param>
        private void OnDoWork(object sender, DoWorkEventArgs e)
        {
            if (!e.Cancel)
            {
                var backgroundWorker = (BackgroundWorker)sender;
                var connectWindow = (ConnectWindowDto)e.Argument;

                // Connexion au serveur
                using var tcpClient = new TcpClient(connectWindow.Hostname, connectWindow.Port);
                var server = new TcpClientServer(tcpClient);

                // Déclaration des gestionnaires d'événements
                EventHandler onClosed = (sender, e) => tcpClient.Close();
                SendToEventHandler onSendTo = (sender, e) => server.SendTo(e.To, e.Message);
                PushEventHandler onPush = (sender, e) => backgroundWorker.ReportProgress(default, new PushEvent(sender, e));
                ReceiveEventHandler onReceive = (sender, e) => backgroundWorker.ReportProgress(default, new ReceiveEvent(sender, e));
                ReceiveToEventHandler onReceiveTo = (sender, e) => backgroundWorker.ReportProgress(default, new ReceiveToEvent(sender, e));

                // Abonnement aux événements
                Closed += onClosed;
                SendTo += onSendTo;
                server.Push += onPush;
                server.Receive += onReceive;
                server.ReceiveTo += onReceiveTo;

                try
                {
                    server.SendName(connectWindow.Name);
                    server.ReadMessages();
                }
                finally
                {
                    // Désabonnement aux événements
                    server.ReceiveTo -= onReceiveTo;
                    server.Receive -= onReceive;
                    server.Push -= onPush;
                    SendTo -= onSendTo;
                    Closed -= onClosed;
                }
            }
        }

        /// <summary>
        /// Gestionnaire d'événement de progrès de la tâche d'arrière plan.
        /// </summary>
        /// <param name="sender">Objet à l'origine de l'événement.</param>
        /// <param name="e">Arguments de l'événement.</param>
        private void OnProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            var serverEvent = (IServerEvent)e.UserState;
            var visitor = new MainWindowServerEventVisitor(Model);
            serverEvent.Accept(visitor);
        }

        /// <summary>
        /// Gestionnaire d'événement de la fin de l'exécution de la tâche d'arrière plan.
        /// </summary>
        /// <param name="sender">Objet à l'origine de l'événement.</param>
        /// <param name="e">Arguments de l'événement.</param>
        private void OnRunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            Model.Connected = false;
            Model.Clients = Model.Clients.Clear();
            Model.Messages = Enumerable.Empty<string>();

            if (e.Error != null)
            {
                MessageBox.Show(e.Error.Message);
            }
        }

        /// <summary>
        /// Gestionnaire d'événement de l'envoie d'un message.
        /// </summary>
        /// <param name="sender">Objet à l'origine de l'événement.</param>
        /// <param name="e">Arguments de l'événement.</param>
        private void OnSend(object sender, RoutedEventArgs e)
        {
            var to = Model.Clients
                .Where(kvp => kvp.Value.SendTo)
                .Select(kvp => kvp.Key);

            SendTo?.Invoke(this, new SendToEventArgs(to, Model.Message));

            Model.Message = string.Empty;
        }

        /// <summary>
        /// Gestionnaire d'événement de la sélection de tous les clients en ligne.
        /// </summary>
        /// <param name="sender">Objet à l'origine de l'événement.</param>
        /// <param name="e">Arguments de l'événement.</param>
        private void OnSelectAll(object sender, RoutedEventArgs e)
        {
            foreach (var client in Model.Clients.Values)
            {
                client.SendTo = true;
            }
        }

        /// <summary>
        /// Gestionnaire d'événement de la désélection de tous les clients en ligne.
        /// </summary>
        /// <param name="sender">Objet à l'origine de l'événement.</param>
        /// <param name="e">Arguments de l'événement.</param>
        private void OnUnselectAll(object sender, RoutedEventArgs e)
        {
            foreach (var client in Model.Clients.Values)
            {
                client.SendTo = false;
            }
        }
    }
}
