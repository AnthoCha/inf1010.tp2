﻿using Inf1010.Tp2.Client.Core.Events;
using Inf1010.Tp2.Client.Core.Interfaces;
using Inf1010.Tp2.Client.Models;
using System.Collections.Immutable;

namespace Inf1010.Tp2.Client.Visitors
{
    /// <summary>
    /// Visiteur d'événements provenant du serveur pour la fenêtre principale.
    /// </summary>
    public class MainWindowServerEventVisitor : IServerEventVisitor
    {
        private readonly MainWindowModel _model;

        /// <summary>
        /// Initialise un visiteur avec le modèle de la fenêtre principale spécifié.
        /// </summary>
        /// <param name="model">Modèle de la fenêtre principale.</param>
        public MainWindowServerEventVisitor(MainWindowModel model)
        {
            _model = model;
        }

        /// <inheritdoc/>
        public void Visit(PushEvent e)
        {
            var clients = _model.Clients;

            _model.Clients = e.EventArgs.Clients.ToImmutableDictionary(
                client => client.Id,
                client =>
                {
                    if (!clients.TryGetValue(client.Id, out var model))
                    {
                        model = new ClientModel();
                    }

                    model.Name = client.Name;
                    return model;
                });
        }

        /// <inheritdoc/>
        public void Visit(ReceiveEvent e)
        {
            _model.AppendMessage(e.EventArgs.From, e.EventArgs.Message);
        }

        /// <inheritdoc/>
        public void Visit(ReceiveToEvent e)
        {
            _model.AppendMessage(e.EventArgs.From, e.EventArgs.Message, e.EventArgs.To);
        }
    }
}
