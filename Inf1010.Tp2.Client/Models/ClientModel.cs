﻿using System.Windows;

namespace Inf1010.Tp2.Client.Models
{
    /// <summary>
    /// Modèle de dépendance d'un client.
    /// </summary>
    public class ClientModel : DependencyObject
    {
        /// <summary>
        /// Obtient et définit le nom du client.
        /// </summary>
        public string Name
        {
            get
            {
                return (string)GetValue(NameProperty);
            }
            set
            {
                SetValue(NameProperty, value);
            }
        }

        /// <summary>
        /// Obtient et définit si le client est sélectionné à l'envoie d'un message.
        /// </summary>
        public bool SendTo
        {
            get
            {
                return (bool)GetValue(SendToProperty);
            }
            set
            {
                SetValue(SendToProperty, value);
            }
        }

        /// <summary>
        /// Propriété de dépendance de <see cref="Name"/>.
        /// </summary>
        public static readonly DependencyProperty NameProperty = DependencyProperty.Register(nameof(Name), typeof(string), typeof(ClientModel), new PropertyMetadata(string.Empty));
        /// <summary>
        /// Propriété de dépendance de <see cref="SendTo"/>.
        /// </summary>
        public static readonly DependencyProperty SendToProperty = DependencyProperty.Register(nameof(SendTo), typeof(bool), typeof(ClientModel), new PropertyMetadata(false));
    }
}
