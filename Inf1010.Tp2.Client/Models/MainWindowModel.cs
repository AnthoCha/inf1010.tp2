﻿using Inf1010.Tp2.Client.Extensions;
using Inf1010.Tp2.Resources;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using System.Text;
using System.Windows;

namespace Inf1010.Tp2.Client.Models
{
    /// <summary>
    /// Modèle de dépendance de la fenêtre principale.
    /// </summary>
    public class MainWindowModel : DependencyObject
    {
        /// <summary>
        /// Obtient et définit les clients en ligne.
        /// </summary>
        public IImmutableDictionary<uint, ClientModel> Clients
        {
            get
            {
                return (IImmutableDictionary<uint, ClientModel>)GetValue(ClientsProperty);
            }
            set
            {
                SetValue(ClientsProperty, value);
            }
        }

        /// <summary>
        /// Obtient et définit les message reçus.
        /// </summary>
        public IEnumerable<string> Messages
        {
            get
            {
                return (IEnumerable<string>)GetValue(MessagesProperty);
            }
            set
            {
                SetValue(MessagesProperty, value);
            }
        }

        /// <summary>
        /// Obtient et définit si le client est connecté.
        /// </summary>
        public bool Connected
        {
            get
            {
                return (bool)GetValue(ConnectedProperty);
            }
            set
            {
                SetValue(ConnectedProperty, value);
            }
        }

        /// <summary>
        /// Obtient et définit le message en cours d'édition.
        /// </summary>
        public string Message
        {
            get
            {
                return (string)GetValue(MessageProperty);
            }
            set
            {
                SetValue(MessageProperty, value);
            }
        }

        /// <summary>
        /// Propriété de dépendance de <see cref="Clients"/>
        /// </summary>
        public static readonly DependencyProperty ClientsProperty = DependencyProperty.Register(nameof(Clients), typeof(IImmutableDictionary<uint, ClientModel>), typeof(MainWindowModel), new PropertyMetadata(ImmutableDictionary.Create<uint, ClientModel>()));
        /// <summary>
        /// Propriété de dépendance de <see cref="Messages"/>
        /// </summary>
        public static readonly DependencyProperty MessagesProperty = DependencyProperty.Register(nameof(Messages), typeof(IEnumerable<string>), typeof(MainWindowModel), new PropertyMetadata(Enumerable.Empty<string>()));
        /// <summary>
        /// Propriété de dépendance de <see cref="Connected"/>
        /// </summary>
        public static readonly DependencyProperty ConnectedProperty = DependencyProperty.Register(nameof(Connected), typeof(bool), typeof(MainWindowModel), new PropertyMetadata(false));
        /// <summary>
        /// Propriété de dépendance de <see cref="Message"/>
        /// </summary>
        public static readonly DependencyProperty MessageProperty = DependencyProperty.Register(nameof(Message), typeof(string), typeof(MainWindowModel), new PropertyMetadata(string.Empty));

        /// <summary>
        /// Ajoute le message au modèle.
        /// </summary>
        /// <param name="from">Identificateur de l'émetteur du message.</param>
        /// <param name="message">Contenu du message.</param>
        public void AppendMessage(uint from, string message)
        {
            AppendMessage(from, message, Labels.Everyone);
        }

        /// <summary>
        /// Ajoute le message au modèle.
        /// </summary>
        /// <param name="from">Identificateur de l'émetteur du message.</param>
        /// <param name="message">Contenu du message.</param>
        /// <param name="to">Collection des identificateurs des destinataires du message.</param>
        public void AppendMessage(uint from, string message, IEnumerable<uint> to)
        {
            AppendMessage(from, message, string.Join(", ", Clients.GetToNames(to)));
        }

        /// <summary>
        /// Ajoute le message au modèle.
        /// </summary>
        /// <param name="from">Identificateur de l'émetteur du message.</param>
        /// <param name="message">Contenu du message.</param>
        /// <param name="toNames">Les noms des destinataires du message.</param>
        private void AppendMessage(uint from, string message, string toNames)
        {
            var sb = new StringBuilder();
            sb.Append(Clients.TryGetValue(from, out var fromClient) ? fromClient.Name : Labels.Unknown);
            sb.Append(' ');
            sb.Append(Labels.To);
            sb.Append(' ');
            sb.Append(toNames);
            sb.Append(": ");
            sb.Append(message);

            Messages = Messages.Append(sb.ToString());
        }
    }
}
