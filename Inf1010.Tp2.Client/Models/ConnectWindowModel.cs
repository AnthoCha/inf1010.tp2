﻿using Inf1010.Tp2.Client.Dtos;
using Inf1010.Tp2.Core.Interfaces;
using System.Windows;

namespace Inf1010.Tp2.Client.Models
{
    /// <summary>
    /// Modèle de dépendance de la fenêtre de connexion.
    /// </summary>
    public class ConnectWindowModel : DependencyObject, IToDto<ConnectWindowDto>
    {
        /// <summary>
        /// Obtient et définit le nom de l'hôte.
        /// </summary>
        public string Hostname
        {
            get
            {
                return (string)GetValue(HostnameProperty);
            }
            set
            {
                SetValue(HostnameProperty, value);
            }
        }

        /// <summary>
        /// Obtient et définit le port de l'hôte.
        /// </summary>
        public int Port
        {
            get
            {
                return (int)GetValue(PortProperty);
            }
            set
            {
                SetValue(PortProperty, value);
            }
        }

        /// <summary>
        /// Obtient et définit le nom d'utilisateur.
        /// </summary>
        public string Name
        {
            get
            {
                return (string)GetValue(NameProperty);
            }
            set
            {
                SetValue(NameProperty, value);
            }
        }

        /// <summary>
        /// Propriété de dépendance de <see cref="Hostname"/>
        /// </summary>
        public static readonly DependencyProperty HostnameProperty = DependencyProperty.Register(nameof(Hostname), typeof(string), typeof(ConnectWindowModel), new PropertyMetadata(string.Empty));
        /// <summary>
        /// Propriété de dépendance de <see cref="Port"/>
        /// </summary>
        public static readonly DependencyProperty PortProperty = DependencyProperty.Register(nameof(Port), typeof(int), typeof(ConnectWindowModel), new PropertyMetadata(0));
        /// <summary>
        /// Propriété de dépendance de <see cref="Name"/>
        /// </summary>
        public static readonly DependencyProperty NameProperty = DependencyProperty.Register(nameof(Name), typeof(string), typeof(ConnectWindowModel), new PropertyMetadata(string.Empty));

        /// <inheritdoc/>
        public ConnectWindowDto Dto
        {
            get
            {
                return new ConnectWindowDto(Hostname, Port, Name);
            }
        }
    }
}
