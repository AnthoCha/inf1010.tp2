﻿using Inf1010.Tp2.Client.Models;
using System.Collections.Generic;

namespace Inf1010.Tp2.Client.Extensions
{
    public static class ClientsExtensions
    {
        /// <summary>
        /// Obtient les noms des utilisateurs par identificateur.
        /// </summary>
        /// <param name="clients">Collection de clés et valeurs des clients par identificateur.</param>
        /// <param name="to">Collection d'identificateurs de clients.</param>
        /// <returns>Retourne les noms des utilisateurs trouvés.</returns>
        public static IEnumerable<string> GetToNames(this IReadOnlyDictionary<uint, ClientModel> clients, IEnumerable<uint> to)
        {
            foreach (var toId in to)
            {
                if (clients.TryGetValue(toId, out var toClient))
                {
                    yield return toClient.Name;
                }
            }
        }
    }
}
