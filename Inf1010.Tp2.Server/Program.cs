using Inf1010.Tp2.Server.Dal.EfCore;
using Inf1010.Tp2.Server.Services;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System.Net;
using System.Net.Sockets;

namespace Inf1010.Tp2.Server
{
    public class Program
    {
        public static void Main(string[] args)
        {
            CreateHostBuilder(args).Build().Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args)
        {
            return Host.CreateDefaultBuilder(args)
                .ConfigureServices((hostContext, services) =>
                {
                    // Obtient le port du serveur de la configuration
                    var port = int.Parse(hostContext.Configuration.GetSection("TcpListener")["Port"]);
                    var tcpListener = new TcpListener(IPAddress.Loopback, port);

                    services.AddSingleton(tcpListener);
                    // Utilise une base de donn�es en m�moire, peut installer un package EfCore diff�rent pour utiliser une vraie base de donn�es
                    services.AddDbContextFactory<ClientDbContext>(options => options.UseInMemoryDatabase("ClientDatabase"));
                    services.AddHostedService<TcpListenerService>();
                });
        }
    }
}
