using Inf1010.Tp2.Server.Core.Interfaces;
using Inf1010.Tp2.Server.Dal.EfCore;
using Inf1010.Tp2.Server.Tcp;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System.Collections.Concurrent;
using System.Linq;
using System.Net.Sockets;
using System.Threading;
using System.Threading.Tasks;

namespace Inf1010.Tp2.Server.Services
{
    /// <summary>
    /// T�che d'arri�re plan prenant en charge un serveur TCP.
    /// </summary>
    public class TcpListenerService : BackgroundService
    {
        private readonly ILogger<TcpListenerService> _logger;
        private readonly ILoggerFactory _loggerFactory;
        private readonly TcpListener _tcpListener;
        private readonly IDbContextFactory<ClientDbContext> _contextFactory;

        /// <summary>
        /// Initialise une instance de <see cref="TcpListenerService"/>.
        /// </summary>
        /// <param name="logger">Logger du service.</param>
        /// <param name="loggerFactory">Fabrique de logger.</param>
        /// <param name="tcpListener">Serveur TCP.</param>
        /// <param name="contextFactory">Frabrique de contexte de base de donn�es.</param>
        public TcpListenerService(ILogger<TcpListenerService> logger, ILoggerFactory loggerFactory, TcpListener tcpListener, IDbContextFactory<ClientDbContext> contextFactory)
        {
            _logger = logger;
            _loggerFactory = loggerFactory;
            _tcpListener = tcpListener;
            _contextFactory = contextFactory;
        }

        /// <inheritdoc/>
        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            // Obtient le contexte de base de donn�es et intialise la couche d'acc�s aux donn�es
            await using var context = _contextFactory.CreateDbContext();
            var repository = new EfCoreClientRepository(context);

            var clients = new ConcurrentDictionary<uint, IClient>();

            _tcpListener.Start();

            var backgroundServiceTasks = Enumerable.Empty<Task>();

            while (!stoppingToken.IsCancellationRequested)
            {
                // Accepte un nouveau client et d�marre une t�che en arri�re plan pour le prendre en charge
                var tcpClient = await _tcpListener.AcceptTcpClientAsync();
                backgroundServiceTasks = backgroundServiceTasks.Append(Task.Run(async () =>
                {
                    using (tcpClient)
                    {
                        var client = new TcpClientClient(tcpClient);
                        using var service = new ClientService(_loggerFactory.CreateLogger<ClientService>(), client, repository, clients);
                        await service.StartAsync(stoppingToken);
                    }
                }, stoppingToken));
            }

            _tcpListener.Stop();

            // Attend la fin des t�ches en arri�re plan
            await Task.WhenAll(backgroundServiceTasks);
        }
    }
}
