﻿using Inf1010.Tp2.Core.Events;
using Inf1010.Tp2.Server.Core.Interfaces;
using Inf1010.Tp2.Server.Core.Interfaces.Repositories;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Concurrent;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Inf1010.Tp2.Server.Services
{
    /// <summary>
    /// Tâche d'arrière plan prenant en charge un client.
    /// </summary>
    public class ClientService : BackgroundService
    {
        private readonly ILogger<ClientService> _logger;
        private readonly IClient _client;
        private readonly IClientRepository _repository;
        private readonly ConcurrentDictionary<uint, IClient> _clients;

        private uint _id;

        /// <summary>
        /// Initialise une instance de <see cref="ClientService"/>.
        /// </summary>
        /// <param name="logger">Logger du service.</param>
        /// <param name="client">Client à prendre en charge.</param>
        /// <param name="repository">Couche d'accès aux données de clients.</param>
        /// <param name="clients">Collection de clients en ligne.</param>
        public ClientService(ILogger<ClientService> logger, IClient client, IClientRepository repository, ConcurrentDictionary<uint, IClient> clients)
        {
            _logger = logger;
            _client = client;
            _repository = repository;
            _clients = clients;
        }

        /// <inheritdoc/>
        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            try
            {
                var name = _client.ReadName();

                // Ajoute le client à la couche d'accès aux données et la collection de clients en ligne
                _id = await _repository.CreateAsync(name);
                _clients[_id] = _client;

                // Abonnement aux événements
                _client.Pull += OnPull;
                _client.Send += OnSend;
                _client.SendTo += OnSendTo;

                // Notifie les clients en ligne de la connexion d'un client
                await PushAsync();

                _client.ReadMessages();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, default);
            }
            finally
            {
                // Désabonnement aux événements
                _client.SendTo -= OnSendTo;
                _client.Send -= OnSend;
                _client.Pull -= OnPull;

                // Retire le client de la couche d'accès aux données et la collection clients en ligne
                if (_clients.TryRemove(_id, out _))
                {
                    await _repository.DeleteAsync(_id);
                    // Notifie les clients en ligne de la déconnexion d'un client
                    await PushAsync();
                }
            }
        }

        /// <summary>
        /// Gestionnaire d'événement de la demande de la collection des clients en ligne.
        /// </summary>
        /// <param name="sender">Objet à l'origine de l'événement.</param>
        /// <param name="e">Arguments de l'événement.</param>
        private async void OnPull(object sender, EventArgs e)
        {
            _client.Push(await _repository.GetAsync());
        }

        /// <summary>
        /// Gestionnaire d'événement de l'envoie d'un message à tous.
        /// </summary>
        /// <param name="sender">Objet à l'origine de l'événement.</param>
        /// <param name="e">Arguments de l'événement.</param>
        private void OnSend(object sender, SendEventArgs e)
        {
            Parallel.ForEach(_clients, kvp => kvp.Value.Retransmit(_id, Enumerable.Empty<uint>(), e.Message));
        }

        /// <summary>
        /// Gestionnaire d'événement de l'envoie d'un message à une collection de destinataires.
        /// </summary>
        /// <param name="sender">Objet à l'origine de l'événement.</param>
        /// <param name="e">Arguments de l'événement.</param>
        private void OnSendTo(object sender, SendToEventArgs e)
        {
            // Ajoute l'identificateur du client aux destinataires de la retransmission du message
            var toIds = e.To.ToHashSet();
            toIds.Add(_id);

            Parallel.ForEach(toIds, toId =>
            {
                if (_clients.TryGetValue(toId, out var toClient))
                {
                    toClient.Retransmit(_id, e.To, e.Message);
                }
            });
        }

        /// <summary>
        /// Gestionnaire d'événement de l'envoie des clients en ligne à tous les clients.
        /// </summary>
        /// <param name="sender">Objet à l'origine de l'événement.</param>
        /// <param name="e">Arguments de l'événement.</param>
        private async Task PushAsync()
        {
            var clients = await _repository.GetAsync();
            Parallel.ForEach(_clients, kvp => kvp.Value.Push(clients));
        }
    }
}
