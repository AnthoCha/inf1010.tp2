﻿using Inf1010.Tp2.Core.Dtos;
using Inf1010.Tp2.Core.Interfaces;

namespace Inf1010.Tp2.Server.Dal.EfCore.Entities
{
    /// <summary>
    /// Enregistrement d'un client.
    /// </summary>
    public class ClientEntity : IToDto<ClientDto>
    {
        /// <summary>
        /// Identificateur du client.
        /// </summary>
        public uint Id { get; set; }
        /// <summary>
        /// Nom d'utilisateur du client.
        /// </summary>
        public string Name { get; set; } = string.Empty;

        /// <inheritdoc/>
        public ClientDto Dto
        {
            get
            {
                return new ClientDto(Id, Name);
            }
        }
    }
}
