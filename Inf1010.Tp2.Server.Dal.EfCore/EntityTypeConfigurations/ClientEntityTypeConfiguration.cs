﻿using Inf1010.Tp2.Server.Dal.EfCore.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Inf1010.Tp2.Server.Dal.EfCore.EntityTypeConfigurations
{
    /// <summary>
    /// Configuration de la table de clients.
    /// </summary>
    public class ClientEntityTypeConfiguration : IEntityTypeConfiguration<ClientEntity>
    {
        /// <inheritdoc/>
        public void Configure(EntityTypeBuilder<ClientEntity> builder)
        {
            builder.ToTable("Client");

            builder.HasKey(entity => entity.Id)
                .HasName("PK_Client");

            builder.Property(entity => entity.Id)
                .ValueGeneratedOnAdd();

            builder.Property(entity => entity.Name)
                .HasMaxLength(64)
                .IsRequired();
        }
    }
}
