﻿using Inf1010.Tp2.Core.Dtos;
using Inf1010.Tp2.Server.Core.Interfaces.Repositories;
using Inf1010.Tp2.Server.Dal.EfCore.Entities;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Inf1010.Tp2.Server.Dal.EfCore
{
    /// <summary>
    /// Implémentation de l'accès aux données utilisant <see cref="EfCore"/>.
    /// </summary>
    public class EfCoreClientRepository : IClientRepository
    {
        private readonly ClientDbContext _context;

        /// <summary>
        /// Initialise une nouvelles instance de <see cref="EfCoreClientRepository"/>
        /// </summary>
        /// <param name="context">Contexte de base de données de clients.</param>
        public EfCoreClientRepository(ClientDbContext context)
        {
            _context = context;
        }

        /// <inheritdoc/>
        public async Task<IEnumerable<ClientDto>> GetAsync()
        {
            return await _context.Clients.Select(entity => entity.Dto).ToListAsync();
        }

        /// <inheritdoc/>
        public async Task<uint> CreateAsync(string name)
        {
            var entity = new ClientEntity
            {
                Name = name
            };

            _context.Clients.Add(entity);

            await _context.SaveChangesAsync();

            return entity.Id;
        }

        /// <inheritdoc/>
        public async Task DeleteAsync(uint id)
        {
            var entity = await _context.Clients.FirstOrDefaultAsync(entity => entity.Id == id);

            if (entity != null)
            {
                _context.Clients.Remove(entity);
                await _context.SaveChangesAsync();
            }
        }
    }
}
