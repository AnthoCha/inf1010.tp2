﻿using Inf1010.Tp2.Server.Dal.EfCore.Entities;
using Inf1010.Tp2.Server.Dal.EfCore.EntityTypeConfigurations;
using Microsoft.EntityFrameworkCore;

namespace Inf1010.Tp2.Server.Dal.EfCore
{
    /// <summary>
    /// Contexte de base de données de clients.
    /// </summary>
    public class ClientDbContext : DbContext
    {
        /// <summary>
        /// Initialise une instance de <see cref="ClientDbContext"/>
        /// </summary>
        /// <param name="options">Options d'initialisation du contexte.</param>
        public ClientDbContext(DbContextOptions<ClientDbContext> options)
            : base(options)
        {

        }

        /// <summary>
        /// Table de clients.
        /// </summary>
        public DbSet<ClientEntity> Clients { get; set; }

        /// <inheritdoc/>
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            new ClientEntityTypeConfiguration().Configure(modelBuilder.Entity<ClientEntity>());
        }
    }
}
