﻿using Inf1010.Tp2.Client.Core.Events;
using Inf1010.Tp2.Client.Core.Interfaces;
using Inf1010.Tp2.Core.Dtos;
using Inf1010.Tp2.Core.Enums;
using Inf1010.Tp2.IO;
using Inf1010.Tp2.IO.Enums;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;

namespace Inf1010.Tp2.Client.Tcp
{
    /// <summary>
    /// Implémentation d'un serveur à partir d'un <see cref="TcpClient"/>.
    /// </summary>
    public class TcpClientServer : IServer
    {
        private readonly TcpClient _tcpClient;
        private readonly object _writeLock = new();

        /// <inheritdoc/>
        public event PushEventHandler Push;
        /// <inheritdoc/>
        public event ReceiveEventHandler Receive;
        /// <inheritdoc/>
        public event ReceiveToEventHandler ReceiveTo;

        /// <summary>
        /// Initialise un serveur avec le <see cref="TcpClient"/> spécifié.
        /// </summary>
        /// <param name="tcpClient">Client TCP.</param>
        public TcpClientServer(TcpClient tcpClient)
        {
            _tcpClient = tcpClient;
        }

        /// <inheritdoc/>
        public void ReadMessages()
        {
            while (_tcpClient.Connected)
            {
                var stream = _tcpClient.GetStream();
                using var reader = new EndianBinaryReader(stream, Encoding.UTF8, true, Endianness.Big);

                // Lit le type de message avant de lire le reste
                var messageType = (MessageType)reader.ReadByte();

                switch (messageType)
                {
                    case MessageType.Message:
                        // Lit les destinataires du message
                        var from = reader.ReadUInt32();
                        var toLength = reader.ReadInt32();
                        var to = new uint[toLength];

                        for (var i = 0; i < toLength; i++)
                        {
                            to[i] = reader.ReadUInt32();
                        }

                        // Lit le contenu du message
                        var message = reader.ReadString();

                        if (toLength > 0)
                        {
                            ReceiveTo?.Invoke(this, new ReceiveToEventArgs(from, to, message));
                        }
                        else
                        {
                            Receive?.Invoke(this, new ReceiveEventArgs(from, message));
                        }
                        break;
                    case MessageType.Clients:
                        var clientsLength = reader.ReadInt32();
                        var clients = new ClientDto[clientsLength];

                        uint id;
                        string name;

                        for (var i = 0; i < clientsLength; i++)
                        {
                            id = reader.ReadUInt32();
                            name = reader.ReadString();
                            clients[i] = new ClientDto(id, name);
                        }

                        Push?.Invoke(this, new PushEventArgs(clients));
                        break;
                }
            }
        }

        /// <inheritdoc/>
        public void SendName(string name)
        {
            var stream = _tcpClient.GetStream();

            lock (_writeLock)
            {
                using var writer = new EndianBinaryWriter(stream, Encoding.UTF8, true, Endianness.Big);

                writer.Write(name);
            }
        }

        /// <inheritdoc/>
        public void SendTo(IEnumerable<uint> to, string message)
        {
            var stream = _tcpClient.GetStream();

            lock (_writeLock)
            {
                using var writer = new EndianBinaryWriter(stream, Encoding.UTF8, true, Endianness.Big);

                writer.Write(to.Count());

                foreach (var toId in to)
                {
                    writer.Write(toId);
                }

                writer.Write(message);
            }
        }
    }
}
