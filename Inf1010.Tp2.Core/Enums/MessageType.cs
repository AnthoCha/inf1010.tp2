﻿namespace Inf1010.Tp2.Core.Enums
{
    /// <summary>
    /// Types de messages.
    /// </summary>
    public enum MessageType : byte
    {
        /// <summary>
        /// Message sous forme de texte.
        /// </summary>
        Message = 0,
        /// <summary>
        /// Collection de clients en ligne.
        /// </summary>
        Clients = 1
    }
}
