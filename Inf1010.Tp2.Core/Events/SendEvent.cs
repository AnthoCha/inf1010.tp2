﻿using System;

namespace Inf1010.Tp2.Core.Events
{
    /// <summary>
    /// Arguments d'événement de l'envoie d'un message à tous.
    /// </summary>
    public class SendEventArgs : EventArgs
    {
        /// <summary>
        /// Obtient le contenu du message.
        /// </summary>
        public string Message { get; }

        /// <summary>
        /// Initialise une instance de <see cref="SendEventArgs"/>.
        /// </summary>
        /// <param name="message">Contenu du message.</param>
        public SendEventArgs(string message)
        {
            Message = message;
        }
    }

    /// <summary>
    /// Gestionnaire d'événements de l'envoie d'un message à tous.
    /// </summary>
    /// <param name="sender">Objet à l'origine de l'événement.</param>
    /// <param name="e">Arguments de l'événement.</param>
    public delegate void SendEventHandler(object sender, SendEventArgs e);
}
