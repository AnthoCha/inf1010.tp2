﻿using System.Collections.Generic;

namespace Inf1010.Tp2.Core.Events
{
    /// <summary>
    /// Arguments d'événement de l'envoie d'un message à une collection de clients.
    /// </summary>
    public class SendToEventArgs : SendEventArgs
    {
        /// <summary>
        /// Obtient la collection des identificateurs des destinataires du message.
        /// </summary>
        public IEnumerable<uint> To { get; }

        /// <summary>
        /// Initialise une instance de <see cref="SendToEventArgs"/>.
        /// </summary>
        /// <param name="to">Collection des identificateurs des destinataires du message.</param>
        /// <param name="message">Contenu du message.</param>
        public SendToEventArgs(IEnumerable<uint> to, string message) : base(message)
        {
            To = to;
        }
    }

    /// <summary>
    /// Gestionnaire d'événements de l'envoie d'un message à une collection de clients.
    /// </summary>
    /// <param name="sender">Objet à l'origine de l'événement.</param>
    /// <param name="e">Arguments de l'événement.</param>
    public delegate void SendToEventHandler(object sender, SendToEventArgs e);
}
