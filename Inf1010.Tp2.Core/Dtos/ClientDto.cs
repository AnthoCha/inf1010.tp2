﻿namespace Inf1010.Tp2.Core.Dtos
{
    /// <summary>
    /// Objet de transfert de données d'un client.
    /// </summary>
    /// <param name="Id">Identificateur du client.</param>
    /// <param name="Name">Nom d'utilisateur du client.</param>
    public record ClientDto(uint Id, string Name);
}
