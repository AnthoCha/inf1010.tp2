﻿namespace Inf1010.Tp2.Core.Interfaces
{
    /// <summary>
    /// Interface pour obtenir l'objet de transfert de données d'une instance.
    /// </summary>
    /// <typeparam name="T">Type de l'objet de transfert de données.</typeparam>
    public interface IToDto<T>
    {
        /// <summary>
        /// Obtient l'objet de transfert de données de cette instance.
        /// </summary>
        T Dto { get; }
    }
}
