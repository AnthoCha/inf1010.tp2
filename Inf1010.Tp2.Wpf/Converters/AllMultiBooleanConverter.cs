﻿using System;
using System.Globalization;
using System.Linq;
using System.Windows;
using System.Windows.Data;

namespace Inf1010.Tp2.Wpf.Converters
{
    /// <summary>
    /// Convertit une collection de valeurs boolénnes en une simple valeur booléenne indiquant si tous les valeurs sont vraies.
    /// </summary>
    [ValueConversion(typeof(bool), typeof(bool))]
    public class AllMultiBooleanConverter : IMultiValueConverter
    {
        /// <inheritdoc/>
        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            return values.All(value => value is bool booleanValue && booleanValue);
        }

        /// <inheritdoc/>
        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            return Enumerable.Repeat(DependencyProperty.UnsetValue, targetTypes.Length).ToArray();
        }
    }
}
