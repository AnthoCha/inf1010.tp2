﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace Inf1010.Tp2.Wpf.Converters
{
    /// <summary>
    /// Convertit une chaine en valeur booléenne indiquant si la chaine est vide.
    /// </summary>
    [ValueConversion(typeof(string), typeof(bool))]
    public class IsNotEmptyStringConverter : IValueConverter
    {
        /// <inheritdoc/>
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is string stringValue)
            {
                return !string.IsNullOrEmpty(stringValue);
            }

            return DependencyProperty.UnsetValue;
        }

        /// <inheritdoc/>
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return DependencyProperty.UnsetValue;
        }
    }
}
