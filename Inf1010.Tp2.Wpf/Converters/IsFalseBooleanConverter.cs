﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace Inf1010.Tp2.Wpf.Converters
{
    /// <summary>
    /// Convertit une valeur boolénne en son inverse.
    /// </summary>
    [ValueConversion(typeof(bool), typeof(bool))]
    public class IsFalseBooleanConverter : IValueConverter
    {
        /// <inheritdoc/>
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is bool booleanValue)
            {
                return !booleanValue;
            }

            return DependencyProperty.UnsetValue;
        }

        /// <inheritdoc/>
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return Convert(value, targetType, parameter, culture);
        }
    }
}
