﻿namespace Inf1010.Tp2.IO.Enums
{
    public enum Endianness
    {
        Little,
        Big,
    }
}
