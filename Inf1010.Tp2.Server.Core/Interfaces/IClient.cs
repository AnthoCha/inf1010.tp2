﻿using Inf1010.Tp2.Core.Dtos;
using Inf1010.Tp2.Core.Events;
using System;
using System.Collections.Generic;

namespace Inf1010.Tp2.Server.Core.Interfaces
{
    /// <summary>
    /// Interface d'un client pour l'utilisation par un serveur.
    /// </summary>
    public interface IClient
    {
        /// <summary>
        /// Lit le nom d'utilisateur du client.
        /// </summary>
        /// <returns>Retourne le nom d'utilisateur du client.</returns>
        string ReadName();
        /// <summary>
        /// Lit tous les messages provenant du client.
        /// </summary>
        void ReadMessages();

        /// <summary>
        /// Envoie les clients en ligne.
        /// </summary>
        /// <param name="clients">Collection des clients en ligne.</param>
        void Push(IEnumerable<ClientDto> clients);
        /// <summary>
        /// Retransmet un message.
        /// </summary>
        /// <param name="from">Identificateur de l'émetteur du message.</param>
        /// <param name="message">Contenu du message.</param>
        /// <param name="to">Collection des identificateurs des destinataires du message.</param>
        void Retransmit(uint from, IEnumerable<uint> to, string message);

        /// <summary>
        /// Événement d'une demande de la collection des clients en ligne.
        /// </summary>
        event EventHandler Pull;
        /// <summary>
        /// Événement de l'envoie d'un message à tous.
        /// </summary>
        event SendEventHandler Send;
        /// <summary>
        /// Événement de l'envoie d'un message à une collection de destinataires.
        /// </summary>
        event SendToEventHandler SendTo;
    }
}
