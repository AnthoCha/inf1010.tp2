﻿using Inf1010.Tp2.Core.Dtos;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Inf1010.Tp2.Server.Core.Interfaces.Repositories
{
    /// <summary>
    /// Interface d'accès aux données.
    /// </summary>
    public interface IClientRepository
    {
        /// <summary>
        /// Obtient les clients.
        /// </summary>
        /// <returns>Retourne les clients.</returns>
        Task<IEnumerable<ClientDto>> GetAsync();

        /// <summary>
        /// Crée un client et obtient son identificateur.
        /// </summary>
        /// <param name="name">Nom d'utilisateur.</param>
        /// <returns>Retourne l'identificateur du nouveau client.</returns>
        Task<uint> CreateAsync(string name);

        /// <summary>
        /// Retire un client.
        /// </summary>
        /// <param name="id">Identificateur du client.</param>
        Task DeleteAsync(uint id);
    }
}
