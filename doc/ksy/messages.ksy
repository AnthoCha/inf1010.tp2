meta:
  id: messages
  endian: be
seq:
  - id: message
    type: message_type
    repeat: eos
types:
  string_type:
    seq:
      - id: length
        type: s4
      - id: data
        type: str
        size: length
        encoding: UTF-8
  message_to_type:
    seq:
      - id: to_length
        type: s4
      - id: to_id
        type: u4
        repeat: expr
        repeat-expr: to_length
      - id: message
        type: string_type
  client_type:
    seq:
      - id: id
        type: u4
      - id: name
        type: string_type
  clients_type:
    seq:
      - id: clients_length
        type: s4
      - id: client
        type: client_type
        repeat: expr
        repeat-expr: clients_length
  message_type:
    seq:
      - id: type
        type: u1
        enum: message_type_enum
      - id: message_to
        type: message_to_type
        if: type == message_type_enum::message
      - id: clients
        type: clients_type
        if: type == message_type_enum::clients
enums:
  message_type_enum:
    0: message
    1: clients
